# frozen_string_literal: true

class PrivateMessagesController < ApplicationController
  before_action :authenticate_user!

  def create
    @private_message = PrivateMessage.new(private_message_params)
    @private_message.sender_id = current_user.id

    @private_message.broadcast_append_to :private_messages if @private_message.save
  end

  def index
    @user = User.find(params[:user_id])
    @private_messages = PrivateMessage.where(
      '(sender_id = ? AND recipient_id = ?) OR (sender_id = ? AND recipient_id = ?)',\
      current_user.id, @user.id, @user.id, current_user.id
    )
    @private_message = PrivateMessage.new
  end

  private

  def private_message_params
    params.require(:private_message).permit(:body, :recipient_id)
  end
end
