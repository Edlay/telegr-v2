# frozen_string_literal: true

class PrivateMessage < ApplicationRecord
  belongs_to :sender, class_name: 'User', foreign_key: 'sender_id'
  belongs_to :recipient, class_name: 'User', foreign_key: 'recipient_id'

  scope :sorted, -> { order(:id) }
  validates :body, presence: true
end
