# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'registrations' }
  resource :chats, only: :create
  get '/chats/:title', to: 'chats#show', as: :chat
  resource :messages, only: :create
  resources :users, only: [:index] do
    resources :private_messages, only: %i[index create]
  end
  root 'chats#index'
end
