# frozen_string_literal: true

user1 = User.create!(email: 'user1@admin.com', password: 'password123')
User.create!(email: 'user2@admin.com', password: 'password123')
User.create!(email: 'user3@admin.com', password: 'password123')

Chat.create!(user: user1)
