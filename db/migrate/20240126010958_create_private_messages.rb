# frozen_string_literal: true

class CreatePrivateMessages < ActiveRecord::Migration[7.1]
  def change
    create_table :private_messages do |t|
      t.text :body
      t.references :user, foreign_key: { to_table: :users, on_delete: :cascade }
      t.integer :sender_id
      t.integer :recipient_id

      t.timestamps
    end

    add_foreign_key :private_messages, :users, column: :sender_id, on_delete: :cascade
    add_foreign_key :private_messages, :users, column: :recipient_id, on_delete: :cascade
  end
end
