# frozen_string_literal: true

require 'cucumber/rails'
require 'capybara/rails'
require 'capybara/cucumber'
require 'selenium/webdriver'

ActionController::Base.allow_rescue = false

begin
  DatabaseCleaner.strategy = :transaction
rescue NameError
  raise 'You need to add database_cleaner to your Gemfile (in the :test group) if you wish to use it.'
end

Cucumber::Rails::Database.javascript_strategy = :truncation
