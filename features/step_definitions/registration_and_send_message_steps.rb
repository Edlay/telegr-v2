# frozen_string_literal: true

Given('the following user exists:') do |table|
  table.hashes.each do |user_params|
    User.create!(user_params)
  end
end

Given('the user is on the registration page') do
  visit(new_user_registration_path)
end

When('the user fills in the registration form') do
  fill_in('Email', with: 'user_test@admin.com')
  fill_in('Password', with: 'password123')
  click_button('Sign up')
end

Then('the user should be successfully registered') do
  expect(page).to have_content('Private message')
end

When("the user clicks on 'Private message'") do
  click_on('Private message')
end

When("the user clicks on 'user1@admin.com'") do
  click_link('user1@admin.com')
end

When("the user writes 'test message' in the 'Your text' field and click 'send'") do
  fill_in('private_message_body', with: 'test message')
  click_on('Send')
  visit(current_path)
end

Then("the user should see the 'test message' on the screen") do
  expect(page).to have_content('test message')
end
