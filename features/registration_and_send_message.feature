Feature: User interactions with private messages

  Background:
    Given the following user exists:
      | email               | password     |
      | user1@admin.com | password123  |

  Scenario: User can register an account and send a private message
    Given the user is on the registration page
    When the user fills in the registration form
    Then the user should be successfully registered

    When the user clicks on 'Private message'
    And the user clicks on 'user1@admin.com'
    And the user writes 'test message' in the 'Your text' field and click 'send'
    Then the user should see the 'test message' on the screen