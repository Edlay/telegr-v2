# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Message, type: :model do
  let(:user) { User.create(email: 'test@example.com', password: 'password123') }
  let(:chat) { Chat.create(title: 'Test Chat', user: user) }
  let(:message1) { Message.create(body: 'Hello', chat: chat, user: user) }
  let(:message2) { Message.create(body: 'Hi', chat: chat, user: user) }

  it 'user relation' do
    expect(message1.user).to eq(user)
  end

  it 'sorted by id' do
    expect(Message.sorted).to eq([message1, message2])
  end

  it 'chat relation' do
    expect(message1.chat).to eq(chat)
  end
end
