# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Chat, type: :model do
  let(:user) { User.create(email: 'test@example.com', password: 'password123') }
  let(:chat) { Chat.create(title: 'Test Chat', user: user) }
  let(:message1) { Message.create(body: 'Hello', chat: chat, user: user) }
  let(:message2) { Message.create(body: 'Hi', chat: chat, user: user) }

  it 'relation user' do
    expect(chat.user).to eq(user)
  end

  it 'has many messages' do
    expect(chat.messages).to eq([message1, message2])
  end
end
