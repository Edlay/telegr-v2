# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PrivateMessage, type: :model do
  let(:sender) { User.create(email: 'sender@example.com', password: 'password123') }
  let(:recipient) { User.create(email: 'recipient@example.com', password: 'password456') }
  let(:private_message1) { PrivateMessage.create(body: 'Hello', sender: sender, recipient: recipient) }
  let(:private_message2) { PrivateMessage.create(body: 'Hi', sender: sender, recipient: recipient) }

  it 'relation sender and recipient' do
    expect(private_message1.sender).to eq(sender)
    expect(private_message1.recipient).to eq(recipient)
  end

  it 'sorted by id' do
    expect(PrivateMessage.sorted).to eq([private_message1, private_message2])
  end

  it 'requires a message text' do
    private_message = PrivateMessage.create(sender: sender, recipient: recipient)

    expect(private_message.errors[:body]).to include("can't be blank")
  end
end
