# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PrivateMessagesController, type: :controller do
  let(:user) { User.create(email: 'test@example.com', password: 'password123') }
  let(:recipient) { User.create(email: 'test2@example.com', password: 'password123') }

  describe 'POST #create' do
    it 'creates a new private message for the current user' do
      sign_in user
      expect do
        post :create, params: { user_id: recipient.id, private_message: { body: 'Hello', recipient_id: recipient.id } }
      end.to change(PrivateMessage, :count).by(1)
    end
  end

  describe 'GET #index' do
    it 'initializes a new private message' do
      PrivateMessage.create(body: 'Hello', sender: user, recipient: recipient)

      sign_in user
      get :index, params: { user_id: recipient.id }

      expect(assigns(:private_messages)).to be_present
      expect(assigns(:private_messages)).to include(an_object_having_attributes(
                                                      body: 'Hello',
                                                      sender: user,
                                                      recipient: recipient
                                                    ))
    end
  end
end
