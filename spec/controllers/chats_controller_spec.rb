# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ChatsController, type: :controller do
  let!(:user) { User.create(email: 'test@example.com', password: 'password123') }
  let!(:chat) { Chat.create(title: 'Test Chat', user: user) }
  let!(:message) { Message.create(body: 'Hello', chat: chat, user: user) }

  describe 'GET #index' do
    it 'initializes a new chat room' do
      get :index
      expect(assigns(:new_chat)).to be_a_new(Chat)
    end

    it 'initializes all chats' do
      get :index
      expect(assigns(:chats)).to eq([chat])
    end
  end

  describe 'GET #show' do
    it 'returns a successful HTTP status' do
      get :show, params: { title: chat.title }
      expect(response).to have_http_status(:success)
    end

    it 'initializes the chat by title' do
      get :show, params: { title: chat.title }
      expect(assigns(:chat)).to eq(chat)
    end

    it 'initializes all chat messages' do
      get :show, params: { title: chat.title }
      expect(assigns(:messages)).to eq([message])
    end
  end
end
